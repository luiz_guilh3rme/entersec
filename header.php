<?php require_once "libs/compress-html.php"; ?>
<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); ?></title>

	    <!-- Stylesheet -->
	    <link href="https://fonts.googleapis.com/css?family=Quattrocento+Sans:400,700" rel="stylesheet">

	    <!-- Mobile metas -->
	    <meta name="format-detection" content="telephone=no">
	    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">

	    <!-- Favicon -->
	    <link rel="shortcut icon" sizes="16x16" href="<?php echo get_template_directory_uri(); ?>/static/images/logo/favicon.ico" />
	    <!-- iPad com Retina Display -->
	    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_template_directory_uri(); ?>/static/images/logo/apple-touch-icon-144x144.png">
	    <!-- Primeira e segunda geração de iPad -->
	    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_template_directory_uri(); ?>/static/images/logo/apple-touch-icon-72x72.png">

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		
		<?php wp_head(); ?>

		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-41552702-1', 'auto');
		  ga('send', 'pageview');

		</script>
<script>
  window.BulldeskSettings = {
    token: 'd76d400d5193e04690f69e11f1e434e9'
  };

  !function(a,b,c){if('object'!=typeof c){var d=function(){d.c(arguments)};d.q=[],d.c=function(a){d.q.push(a)},a.Bulldesk=d;var e=b.createElement('script');e.type='text/javascript',e.async=!0,e.id='bulldesk-analytics',e.src='https://static.bulldesk.com.br/bulldesk.js';var f=b.getElementsByTagName('script')[0];f.parentNode.insertBefore(e,f)}}(window,document,window.Bulldesk);
</script>


	</head>
	<body <?php body_class(); ?>>

		<!-- HEADER -->
		<header class="main-header" role="header">
			<!-- TOP -->
			<div class="header-top">
			      <div class="container">
			        <div class="row">
			          <!-- MARCA -->
			          <div class="marca col-xs-12 col-md-6">
			            <a href="<?php echo home_url(); ?>" title="Fechaduras Eletrônicas | Entersec">
			              <img src="<?php echo get_template_directory_uri(); ?>/static/images/marca.png" alt="Fechaduras Eletrônicas | Entersec" title="Fechaduras Eletrônicas | Entersec">
			            </a>

			            <i class="menu-toggle-mobile visible-xs"></i>
			          </div>

			          <!-- CONTATOS -->
			          <div class="info col-xs-12 col-md-6">
			            <div class="contatos">
			              <a href="tel:+55-11-2242-6333" class="icon-fone" onclick='ga("send", "event", "Contato", "click", "Telefone")'>(11) 2242-6333</a>
			              <a href="mailto:andrew@entersec.com.br" class="icon-mail" onclick='ga("send", "event", "Contato", "click", "Email")'>comercial@entersec.com.br</a>
			            </div>

			            <div class="redes">
			              <a href="https://www.instagram.com/_entersec/" title="Instagram Entersec" rel="nofollow" class="icon-instagram"></a>
			              <a href="https://www.facebook.com/Grupo-CF-Entersec-192990334126659/" title="Facebook Entersec" rel="nofollow" class="icon-facebook"></a>
			            </div>
			          </div>
			        </div>
			      </div>
			</div>
			<!-- MENU -->
			<div class="header-nav">
			      <div class="container">
			        <div class="row">
			          <nav class="main-menu dropmenu">
			            <?php html5blank_nav(); ?>
			          </nav>
			        </div>
			      </div>
			</div>
		</header>
		<!-- //HEADER -->
