<div class="banner" role="banner">

  <div class="banner-content swiper-container swiper1">
    <div class="swiper-wrapper">

      <?php wp_reset_query(); ?>
      <?php $banner_query = array( 
        'post_type' => 'banner',
        'posts_per_page' => '999',
        'orderby' => 'date'
      ); ?>
      
      <?php query_posts($banner_query); while (have_posts()) : the_post(); ?>

        <div class="banner-item swiper-slide" style="background-image: url('<?php echo the_post_thumbnail_url('banner'); ?>');">
          <div class="container">
            <div class="row">
              <div class="banner-txt col-xs-12 col-sm-8 col-md-6">
                <h1><?php echo the_title() ?></h1>
                <br/>
                <a href="<?php echo get_post_meta($post->ID, 'link', true);?>" class="icon-seta">
                  <i class="path1"></i><i class="path2"></i>
                  <span>saiba mais</span>
                </a>
              </div>
            </div>
          </div>
        </div>

      <?php endwhile; ?>

    </div>

    <div class="swiper-pagination1 swiper-pagination"></div>
    <div class="seta1">
      <img src="<?php echo get_template_directory_uri(); ?>/static/images/seta.svg">
    </div>
  </div>


</div>