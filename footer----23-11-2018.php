		<footer class="footer" itemscope itemtype="http://schema.org/LocalBusiness">



			<div class="footer-main">

				<div class="container">

					<div class="row">



						<div class="footer-info col-xs-12 col-md-3">

							<div class="footer-marca">

								<img itemprop="image" src="<?php echo get_template_directory_uri(); ?>/static/images/entersec-branca.png" alt="Entersec">

							</div>



							<div class="footer-txt">

								<a href="tel:+55-11-2242-6333" class="icon-fone" itemprop="telephone" content="+551122426333">+55 (11) 2242-6333</a><br/>

								<a href="mailto:comercial@entersec.com.br" class="icon-mail">comercial@entersec.com.br</a>

								<p class="icon-home" itemscope itemtype="http://schema.org/PostalAddress">

									<span itemprop="streetAddress"> Rua Hintem Martins, 473</span> <br/> <span>Pq. Edu Chaves</span><br/>

									<span itemprop="postalCode">CEP 02229-070</span>, <span itemprop="addressRegion">São Paulo - SP</span>

								</p>



								<br/>



								<div class="hora-funcionamento">

									<p>HORÁRIO DE FUNCIONAMENTO</p>

									<p itemprop="openingHours" content="Mo,Tu,We,Th,fr,  09:00-18:00">Segunda a Quinta : 09h as 18h<br/>Sexta - feira : 09h as 17h</p>

								</div>

							</div>

						</div>

						

						<!-- MENU DO FOOTER -->

						<div class="footer-menu col-xs-12 col-md-8 col-md-push-1">

							<div class="non-style-list">

								<?php wp_nav_menu( array( 'theme_location' => 'footer-menu' ) ); ?>

							</div>



						</div>



					</div>

				</div>

			</div>



			<div class="footer-bottom">

				<div class="container">

					<div class="left">

						<p>All rights reserved. <?= date('Y'); ?> ©. <span itemprop="name">Grupo CF Entersec</span>.</p>

					</div>



					<div class="right">

						<a href="https://www.3xceler.com.br/criacao-de-sites">criação de site</a>: <img src="<?php echo get_template_directory_uri(); ?>/static/images/marca-3xceler.png" alt="Agência 3xceler" title="Agência 3xceler">

					</div>

				</div>

			</div>



		</footer>



		<!-- MODAL TE LIGAMOS -->

		<div class="modal" id="modal-teligamos">

			<div class="modalcontainer">

				<h4 class="txt-center">Nós te ligamos</h4>

				<div class="page-form">

					<form action="" id="form-teligamos">



						<!-- NEWSP -->

						<input type="hidden" name="url" value="{$smarty.server.HTTP_HOST}{$smarty.server.REQUEST_URI}">

						<!-- TIPO DE FORM -->

						<input type="hidden" name="tipo" class="tipo" value="teligamos">



						<div class="form-line">

							<div class="form-item">

								<input type="text" name="nome" placeholder="Nome">

							</div>

						</div>



						<div class="form-line">

							<div class="form-item">

								<input type="text" name="fone" class="celular-input" placeholder="Telefone">

							</div>

						</div>



						<!-- submit -->

						<div class="form-line">

							<button class="icon-seta" type="submit">

								<i class="path1"></i><i class="path2"></i>

								<span>Enviar</span>

							</button>

						</div>

					</form>

				</div>

			</div>



			<div class="modal-close">+</div>

		</div>



		<!-- MODAL ORÇAMENTO -->

		<div class="modal" id="modal-orcamento">

			<div class="modalcontainer">

				<h4 class="txt-center">Orçamento online</h4>

				<div class="page-form">

					<form action="" id="form-orcamento">



						<!-- NEWSP -->

						<input type="hidden" name="url" value="{$smarty.server.HTTP_HOST}{$smarty.server.REQUEST_URI}">

						<!-- TIPO DE FORM -->

						<input type="hidden" name="tipo" class="tipo" value="orcamento">



						<div class="form-line">

							<div class="form-item">

								<input type="text" name="nome" placeholder="Nome">

							</div>

						</div>



						<div class="form-line">

							<div class="form-item">

								<input type="email" name="email" placeholder="Email">

								<input type="text" name="fone" class="celular-input" placeholder="Telefone">

							</div>

						</div>



						<?php

						wp_reset_query();



						$produtoscateg  = array(

							'post_type'=>'post',

							'posts_per_page'=> '999'		

						);

						?>



						<div class="form-line">

							<div class="form-item">

								<select name="produto" id="produto">

									<?php query_posts($produtoscateg); while (have_posts()) : the_post(); ?>

									<option value="<?php the_title(); ?>"><?php the_title(); ?></option>

								<?php endwhile; ?>

							</select>

						</div>

					</div>



					<div class="form-line">

						<div class="form-item">

							<textarea name="mensagem" id="mensagem" cols="30" rows="10" placeholder="Mensagem"></textarea>

						</div>

					</div>



					<!-- submit -->

					<div class="form-line" type="submit">

						<button class="icon-seta">

							<i class="path1"></i><i class="path2"></i>

							<span>Enviar</span>

						</button>

					</div>

				</form>

			</div>

		</div>



		<div class="modal-close">+</div>

	</div>

	<!-- WHATSAPP SIDEBAR AND CTA -->
	<a href="https://wa.me/5511947437831" target="_BLANK" class="cta-whatsapp" onclick='ga("send", "event", "Contato", "Click", "Whatsapp")' title="Fale conosco no Whatsapp!">
		<img src="<?php echo get_template_directory_uri(); ?>/static/images/cta-whatsapp.png" alt="Fale conosco no Whatsapp!">
		<span><img src="<?php echo get_template_directory_uri(); ?>/static/images/whats-ico.png" alt="Fale conosco no Whatsapp!">MANDE UM WHATSAPP</span>
	</a>



	<script>

		var path =  "<?php echo get_template_directory_uri(); ?>/libs/PHPMailer/envia.php";

	</script>



	<?php wp_footer(); ?>



	<!-- Código do Google para tag de remarketing -->

	<script type="text/javascript">

		/* <![CDATA[ */

		var google_conversion_id = 867740496;

		var google_custom_params = window.google_tag_params;

		var google_remarketing_only = true;

		/* ]]> */

	</script>

	<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">

	</script>

	<noscript>

		<div style="display:inline;">

			<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/867740496/?guid=ON&amp;script=0"/>

		</div>

	</noscript>


	<script>
		$('.gallery-thumbs .galery-item').click(function(e) {
			var caminho;

			e.preventDefault();
			if ( $(this).hasClass('iframe') ) {
				caminho = $(this).children('iframe').attr('src'); 
				$('.gallery-image').html('<iframe width="100%" height="320" src="'+caminho+'" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>');

			}

			else {
				caminho = $(this).children('img').attr('src');
				$('.gallery-image').html('<img src="'+caminho+'">');
			}
		});
	</script>
</body>

</html>


