<?php get_header(); ?>

<!-- Banner -->
<?php include('banner-interno.php') ?>
<!-- //Banner -->

<main class="main" role="main">

	<section class="section sec-single">
		<div class="container">
			<?php if (have_posts()): while (have_posts()) : the_post(); ?>

				<div class="single-img col-xs-12">
					<div class="row">
						<div class="single-gallery col-xs-12 col-md-6">
							<!-- post thumbnail -->
							<div class="gallery-image">
								<?php $image = get_field('imagem'); ?>
								<?php if( !empty($image) ): ?>
									<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
								<?php endif; ?>
								<!-- /post thumbnail -->
							</div>
							
							<div class="gallery-thumbs">
								<?php if( !empty($image) ): ?>
									<div class="galery-item gallery-item-1">
										<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
									</div>
								<?php endif; ?>

								<?php $image2 = get_field('imagem_2'); ?>
								<?php if( !empty($image2) ): ?>
									<div class="galery-item gallery-item-2">
										<img src="<?php echo $image2['url']; ?>" alt="<?php echo $image2['alt']; ?>" />
									</div>
								<?php endif; ?>

								<?php $image3 = get_field('imagem_3'); ?>
								<?php if( !empty($image3) ): ?>
									<div class="galery-item gallery-item-3">
										<img src="<?php echo $image3['url']; ?>" alt="<?php echo $image3['alt']; ?>" />
									</div>
								<?php endif; ?>

								<?php if ( get_field('video') ) { ?>
									<div class="galery-item gallery-item-4 iframe">
										<iframe style="pointer-events: none;" width="100%" height="108" src="<?php the_field('video'); ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
									</div>
								<?php } ?>
							</div>
						</div>

						<div class="produto-desc col-xs-12 col-md-6">
							<article>
								<h1><?php the_title(); ?></h1>
								<div class="content">
									<?php the_content(); ?>
								</div>
							</article>
						</div>
					</div>
				</div>

				<div class="categ-desc col-xs-12">
					<div class="row">
						<div class="descricao-produto col-xs-12 col-md-6">
							<h3 class="h4">Especificações Técnicas:</h3>
							
							<p>
								<?php 
									/**
									 * 
									 * ONE GAMBIARRA TO RULE THEM ALL
									 * AND IN THE DARKNESS BIND THEM
									 * 
									 */
									$meta_field = get_post_meta( $post->ID, 'descricao', true ); 
									$meta_value = !empty( $meta_field ) ? $meta_field : get_field('descricao', $post->ID);

									if ( $meta_value ) {
										echo $meta_value; 
									}
									?>
								</p>

							</div>
							
							<div class="col-xs-12 col-md-6">
								<div class="botoes">
									<?php if (	get_the_ID() == 402 ): ?>
										
										<a target="_blank" href="https://www.americanas.com.br/produto/106943382/fechadura-digital-biometrica-entersec-acabamento-aluminio-preto-bio501?pfm_carac=bio501&pfm_index=0&pfm_page=search&pfm_pos=grid&pfm_type=search_page%20&sellerId&sellerid">
											<span class="btn-l btn-flat-green">	
												Comprar Agora
											</span>
										</a>
										
										<?php else: ?>
											<span class="modal-trigger btn-l btn-flat-green" name="modal-teligamos">Te ligamos</span>
											<span class="modal-trigger btn-l btn-flat-blue" name="modal-orcamento">Orçamento online</span>
										<?php endif; ?>
									</div>
								</div>
							</div>
						</div>
					<?php endwhile; ?>

					<?php else: ?>

						<!-- article -->
						<article>

							<h1><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h1>

						</article>
						<!-- /article -->

					<?php endif; ?>
				</div>
			</section>

		</main>

		<?php get_footer(); ?>
