<?php /* Template Name: Sucesso */ get_header(); ?>

<!-- Banner -->
<?php include('banner-interno.php') ?>
<!-- //Banner -->

<main class="main" role="main">

	<section class="section page">
		<div class="container">

			<div class="page-text col-xs-12 col-md-12 txt-center">

				<?php if (have_posts()): while (have_posts()) : the_post(); ?>
					<article class="h1">
						<?php the_content(); ?>
					</article>
				<?php endwhile; ?>
				<?php endif; ?>

			</div>

		</div>
	</section>

</main>

<?php

$meta = get_post_meta($post->ID, 'conversion_tag', true);

if(!empty($meta)){
 echo $meta;
}?>

<?php get_footer(); ?>
