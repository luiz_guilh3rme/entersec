<section class="section about-home-section">
  <div class="container">
    <div class="row">

      <?php if ( wp_is_mobile() ): ?>
        <div class="swiper-container swiper-arrow-container swiper2">
            <div class="swiper-wrapper">
      <?php endif ?>
      
              <div class="about-block sec-block swiper-slide col-xs-12 col-md-5">
                <h3 class="block-title h1">Quem <strong>Somos</strong></h3>
                <p>Presente no mercado de equipamentos eletrônicos há 20 anos, a ENTERSEC é uma empresa da Castro Fortunato Comércio de Equipamentos Eletrônicos Ltda, e iniciou suas atividades na montagem de quadros elétricos e na integração de sistemas inteligentes em automação Predial (Edifícios Inteligentes) para, no mesmo ano de sua fundação, tornar-se distribuidora exclusiva no Brasil das fechaduras Eletrônicas Timelox Suécia.</p>
                <a href="http://www.entersec.com.br/sobre-a-entersec/" class="icon-seta">
                  <i class="path1"></i><i class="path2"></i>
                  <span>saiba mais</span>
                </a>
              </div>

              <div class="servicos-block sec-block swiper-slide col-xs-12 col-md-4">
                <h3 class="block-title h1">Outros <strong>serviços</strong></h3>
                <p>E a Entersec não apenas comercializa seus produtos, nós também oferecemos aos nossos clientes diversos serviços para a melhor utilização de cada um de nossos produtos, além de suporte completo. Fazemos a instalação das fechaduras, treinamento operacional, visitas em campo, suporte remoto, contrato de manutenção, manutenção preventiva e reparo de peças em nossa oficina.</p>
                <!-- <a href="http://dev.3xceler.com.br/entersec/sobre-a-entersec/" class="icon-seta">
                  <i class="path1"></i><i class="path2"></i>
                  <span>saiba mais</span>
                </a> -->
              </div>

              <div class="servicos-block sec-block swiper-slide col-xs-12 col-md-3">
                <h3 class="block-title h1"><strong>Suporte</strong></h3>
                <p>A Entersec garante aos seus clientes um pós-venda diferenciado, com sua equipe de profissionais altamente qualificados e preparados para quaisquer eventualidades, oferecendo suporte e qualidade de atendimento para o seu empreendimento. Tire todas as suas dúvidas sobre o funcionamento de nossos sinalizadores, economizadores, cofres e fechaduras eletrônicas por meio deste canal.</p>
                <a href="http://www.entersec.com.br/suporte/" class="icon-seta">
                  <i class="path1"></i><i class="path2"></i>
                  <span>saiba mais</span>
                </a>
              </div>

      <?php if ( wp_is_mobile() ): ?>
          </div>
          <div class="swiper-2-button-prev swiper-arrow swiper-prev"></div>
          <div class="swiper-2-button-next swiper-arrow swiper-next"></div>
        </div>
      <?php endif ?>
    </div>
  </div>
</section>