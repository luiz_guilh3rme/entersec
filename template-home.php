<?php /* Template Name: Home */ get_header(); ?>

	<!-- Banner -->
	<?php include('banner-home.php') ?>
	<!-- //Banner -->

	<main role="main">
		<!-- section produtos home -->
		<?php include('sec-produtos-home.php') ?>
		<!-- //section produtos home -->

		<!-- Section azul -->
		<?php include('sec-blue.php') ?>
		<!-- //Section azul -->

		<!-- Section about -->
		<?php include('sec-about-home.php') ?>
		<!-- //Section about -->

		<!-- Section depoimentos e form -->
		<?php include('sec-depoimento-form.php') ?>
		<!-- //Section depoimentos e form -->
	</main>


<?php get_footer(); ?>
