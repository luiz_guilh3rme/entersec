$(function () {
    jQuery.validator.addMethod("testEmail", function (value, element) {
        return this.optional(element) || /[-0-9a-zA-Z.+_]+@[-0-9a-zA-Z.+_]+\.[a-zA-Z]{2,4}/i.test(value);
    }, "Digite e-mail valido.");

    var SPMaskBehavior = function (val) {
        return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
    }, spOptions = {
        onKeyPress: function (val, e, field, options) {
            field.mask(SPMaskBehavior.apply({}, arguments), options);
        }
    };
    $('.celular-input').mask(SPMaskBehavior, spOptions);
    $('.cep').mask('00000-000');

    var path = "http://www.entersec.com.br/wp-content/themes/Entersec/libs/PHPMailer/envia.php"


    // Validação de Formularios
    $('#form-home').validate({
        rules: {
            email: {
                required: true,
                email: true,
                testEmail: true
            },
            nome: {
                required: true,
                minlength: 2
            }
        }, messages: {
            email: {
                required: "Insira seu email",
                email: "Insira um email válido"
            },
            nome:{
                required: "Insira seu nome",
                minlength: "Insira um nome válido"
            }
        }, submitHandler: function (form) {
            var dados = $(form).serialize();
            var host = path;
            var actBtn = $(form).find('button[type="submit"]');
            var oldText = actBtn.text();

            actBtn.text('Enviando...');
            actBtn.attr('disabled', true);

            $.ajax({
                type: "POST",
                url: host,
                async: true,
                data: dados,

                success: function (data) {
                    var result = data == '' ? data : JSON.parse(data);
                    if (result.status == '200') {
                        // window.location = "sucesso-home";
                        window.location = "obrigado";
                    } else {
                        actBtn.text(oldText);
                        actBtn.attr('disabled', false);
                    }

                },
                error: function (data) {
                    actBtn.text(oldText);
                    actBtn.attr('disabled', false);
                }
            });

            return false;
        }
    });

    $('#form-interna').validate({
        rules: {
            email: {
                required: true,
                email: true,
                testEmail: true
            },
            nome: {
                required: true,
                minlength: 2
            }
        }, messages: {
            email: {
                required: "Insira seu email",
                email: "Insira um email válido"
            },
            nome:{
                required: "Insira seu nome",
                minlength: "Insira um nome válido"
            }
        }, submitHandler: function (form) {
            var dados = $(form).serialize();
            var host = path;
            var actBtn = $(form).find('button[type="submit"]');
            var oldText = actBtn.text();

            actBtn.text('Enviando...');
            actBtn.attr('disabled', true);

            $.ajax({
                type: "POST",
                url: host,
                async: true,
                data: dados,

                success: function (data) {
                    var result = data == '' ? data : JSON.parse(data);
                    if (result.status == '200') {
                        // window.location = "/sucesso-interna";
                        window.location = "sucesso-home";
                    } else {
                        actBtn.text(oldText);
                        actBtn.attr('disabled', false);
                    }

                },
                error: function (data) {
                    actBtn.text(oldText);
                    actBtn.attr('disabled', false);
                }
            });

            return false;
        }
    });    

    // Validação de Formularios
    $('#form-teligamos').validate({
        rules: {
            nome: {
                required: true,
                minlength: 2
            },
            fone: {
                required: true,
                minlength: 14
            }
        }, messages: {
            nome:{
                required: "Insira seu nome",
                minlength: "Insira um nome válido"
            },
            fone:{
                required: "Insira seu telefone",
                minlength: "Insira um telefone válido"
            }
        }, submitHandler: function (form) {
            var dados = $(form).serialize();
            var host = path;
            var actBtn = $(form).find('button[type="submit"]');
            var oldText = actBtn.text();

            actBtn.text('Enviando...');
            actBtn.attr('disabled', true);

            $.ajax({
                type: "POST",
                url: host,
                async: true,
                data: dados,

                success: function (data) {
                    var result = data == '' ? data : JSON.parse(data);
                    if (result.status == '200') {
                        // window.location = "/sucesso-teligamos";
                    window.location = "sucesso-home";
                    } else {
                        actBtn.text(oldText);
                        actBtn.attr('disabled', false);
                    }

                },
                error: function (data) {
                    actBtn.text(oldText);
                    actBtn.attr('disabled', false);
                }
            });

            return false;
        }
    });


    // Validação de Formularios
    $('#form-orcamento').validate({
        rules: {
            email: {
                required: true,
                email: true,
                testEmail: true
            },
            nome: {
                required: true,
                minlength: 2
            },
            fone: {
                required: true,
                minlength: 14
            }
        }, messages: {
            email: {
                required: "Insira seu email",
                email: "Insira um email válido"
            },
            nome:{
                required: "Insira seu nome",
                minlength: "Insira um nome válido"
            },
            fone:{
                required: "Insira seu telefone",
                minlength: "Insira um telefone válido"
            }
        }, submitHandler: function (form) {
            var dados = $(form).serialize();
            var host = path;
            var actBtn = $(form).find('button[type="submit"]');
            var oldText = actBtn.text();

            actBtn.text('Enviando...');
            actBtn.attr('disabled', true);

            $.ajax({
                type: "POST",
                url: host,
                async: true,
                data: dados,

                success: function (data) {
                    var result = data == '' ? data : JSON.parse(data);
                    if (result.status == '200') {
                        // window.location = "/sucesso-orcamento";
                        window.location = "sucesso-home";
                    } else {
                        actBtn.text(oldText);
                        actBtn.attr('disabled', false);
                    }

                },
                error: function (data) {
                    actBtn.text(oldText);
                    actBtn.attr('disabled', false);
                }
            });

            return false;
        }
    });



});
