$(function () {

	//ALTURA DA SECTION DE ECONOMIA
	$('.section.economia-section').each(function(){
		var alturasec = $(this).height();
		var alturamen = alturasec+100;

		$('.economia-section .container .men2').css("height",alturamen+"px");
	});

	// MENU DROPDOWN
	var tamanhotela = $(window).width();

	if( tamanhotela > 991 ){
		$('.dropmenu li').hover(function(){
			$(this).toggleClass('active');
			$('ul', this).stop().slideToggle(300);
		});
	} else {
		$('.dropmenu .sub-menu').after('<div class="toggle-submenu"></div>');
		$('.toggle-submenu').click(function(){
			$(this).siblings('ul').slideToggle(300);
		});
	}

	// MENU MOBILE
	$('.menu-toggle-mobile').click(function(){
		$(this).toggleClass('active');
		$('.header-nav').toggleClass('menu-active');
	});

	function menuposition(){
		var offsetwindow = $(window).scrollTop();
		var larguratela = $(window).width();
		var offsetmarca = 104 - offsetwindow;
		
		if(larguratela < 769){
			if(offsetwindow > 54){
				$('.marca').css("position","fixed");		
				$('.marca').css("top","0");		

				$('.header-nav').css("top","50px");
			}else{
				$('.marca').css("position","absolute");		
				$('.marca').css("top","54px");	

				$('.header-nav').css("top",offsetmarca+"px");
			}
		}

	}

	menuposition();

	$(window).scroll(function(){
		menuposition();
	});

	// Modal
	$('.modal-trigger').click(function(){
		var thismodal = $(this).attr('name');
		$('#'+thismodal).fadeIn(300);
	});

	$('.modal-close').click(function(){
		$(this).parent().fadeOut(300);
	});

	// SELECT DO MODAL - PARA VIR SELECIONADO QUANDO ESCOLHER O PRODUTO
	function displayVals() {
		$('.modal-trigger').click(function( ev ){
		  ev.preventDefault();
		  var textoption = $('article h1').text();
		  $( "#produto" ).val( textoption ).change();
		});
	}

	displayVals();

	// GALERIA
	$('.gallery-thumbs .galery-item img').click(function(){
		var caminho = $(this).attr('src');

		$('.gallery-image img').attr('src',caminho);
	});

	var swiper1 = new Swiper('.swiper-depoimentos', {
	    pagination: '.swiper-depoimentos-pag',
	    // effect: 'fade',
	    paginationClickable: true,
	    autoplay: 5000,
	    autoplayDisableOnInteraction: true,
	    preventClicks: false,
	    spaceBetween: 0
	});

	var swiper1 = new Swiper('.swiper1', {
	    pagination: '.swiper-pagination1',
	    // effect: 'fade',
	    paginationClickable: true,
	    autoplay: 5000,
	    autoplayDisableOnInteraction: true,
	    preventClicks: false,
	    spaceBetween: 0
	});


	  var swiper2 = new Swiper('.swiper2', {
	    paginationClickable: true,
		nextButton: '.swiper-2-button-next',
		prevButton: '.swiper-2-button-prev',
		autoplay: false,
	    preventClicks: false,
		autoplayDisableOnInteraction: true,
		slidesPerView: 1,
		spaceBetween: 40
	});

	  var swiper3 = new Swiper('.swiper3', {
	    paginationClickable: true,
		nextButton: '.swiper-3-button-next',
		prevButton: '.swiper-3-button-prev',
		autoplay: false,
	    preventClicks: false,
		autoplayDisableOnInteraction: true,
		slidesPerView: 'auto',
		spaceBetween: 0
	});

});