<?php /* Template Name: WhatsApp Redirect */ get_header(); ?>

<!-- Banner -->
<?php include('banner-interno.php') ?>
<!-- //Banner -->

<main class="main" role="main">
	<section class="section page">
		<div class="container">
			<div class="page-text col-xs-12 col-md-12 txt-center">
				<article class="h1">
					Redirecionando...
				</article>
			</div>
		</div>
	</section>
</main>

<script>
	setTimeout(function(){
	 window.location.href = 'https://wa.me/5511947437831';
         }, 2000);
</script>


<?php get_footer(); ?>
