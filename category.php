<?php get_header();
$obj = get_queried_object();
$objCat = $obj->term_id; 
 ?>

	<!-- Banner -->
	<?php include('banner-interno.php') ?>
	<!-- //Banner -->

	<main role="main">
		<!-- section -->
		<section class="section sec-categ">
			<div class="container">
				<div class="row">

					<div class="categ-img col-xs-12 col-md-6">
						<!-- <img src="<?php if (function_exists('z_taxonomy_image_url')) echo z_taxonomy_image_url(); ?>" alt="<?php single_cat_title(); ?>" title="<?php single_cat_title(); ?>"> -->
						<iframe src="<?php the_field('category_video', 'category_' . $objCat); ?>" height="425" width="100%" frameborder="0"></iframe>
					</div>

					<div class="categ-desc col-xs-12 col-md-6">
						<h1><?php single_cat_title(); ?></h1>
						<div class="content">
							<?php echo category_description(); ?>
						</div>
					</div>

					<div class="botoes col-xs-12">
						<span class="modal-trigger btn-l btn-flat-green" name="modal-teligamos">Te ligamos</span>
						<span class="modal-trigger btn-l btn-flat-blue" name="modal-orcamento">Orçamento online</span>
					</div>

					<!-- SWIPER DOS PRODUTOS -->
					<div class="produtos-swipe col-xs-12">
						<div class="swiper-container swiper-arrow-container swiper3">
	            			<div class="swiper-wrapper">

	            				<?php
									wp_reset_query();

									$cat = get_query_var('cat');
									$produtoscateg  = array(
										'post_type'=>'post',
										'posts_per_page'=> -1,
										'cat'=>$cat				
									);
								?>

								<?php query_posts($produtoscateg); while (have_posts()) : the_post(); ?>

		            				<div class="swiper-slide produto-item col-xs-12 col-md-6">
		            					<div class="produto-thumb">
		            						<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
		            							<?php the_post_thumbnail('produto');?>
		            						</a>
		            					</div>
		            					<div class="produto-desc">
		            						<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
		            							<h3 class="h4"><?php the_title(); ?></h3>
		            						</a>
		            						<p><?php the_excerpt('post_list'); ?></p>
		            						<div class="produto-link">
		            							<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="icon-seta"><i class="path1"></i><i class="path2"></i><span> saiba mais</span></a>
		            						</div>
		            					</div>
		            				</div>

								<?php endwhile; ?>

	            			</div>
	            		</div>

	            		<div class="swiper-3-button-prev swiper-arrow swiper-prev"></div>
	          			<div class="swiper-3-button-next swiper-arrow swiper-next"></div>
					</div>
					<!-- //SWIPER DOS PRODUTOS -->
						
				</div>
			</div>
		</section>
		<!-- /section -->

		<!-- Section azul -->
		<?php include('sec-blue.php') ?>
		<!-- //Section azul -->

	</main>

<?php get_footer(); ?>
