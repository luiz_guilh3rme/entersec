<section class="section produtos-section">
  <div class="container">
    <div class="row">

      <div class="title-section title-center col-md-12">
        <h2>Nossos <strong>produtos</strong></h2>
        <p>Oferecemos os melhores e mais modernos cofres e fechaduras eletrônicas, além de economizadores de energia para hotéis, hospitais, universidades, pousadas e outros.</p>
      </div>

      <div class="section-content col-xs-12 col-md-10 col-md-push-1">
        
        <div class="sec-block produto-1">
          <div class="block-img">
            <img src="<?php echo get_template_directory_uri(); ?>/static/images/produto-1.png" alt="Sinalizadores externos Entersec" title="Sinalizadores externos Entersec">
          </div>
          <div class="block-text">
            <h4>Sinalizadores externos</h4>
            <p>Dispomos dos mais avançados sinalizadores externos para realizar a confirmação da presença ou não do hóspede no quarto de hotel. Os sinalizadores também oferecem outras comodidades e confortos aos clientes como o modo “não perturbe” com bloqueio de campainha e solicitação de limpeza do quarto. Trabalhamos com os Sinalizadores Touch Screen Modelo NP01, Modelo NP02, Modelo NPLQ01, Modelo NPLQ02, Modelo NPLR01 e Modelo NPLR02, os melhores do ramo em todo o mercado nacional e internacional.</p>
            <a href="http://www.entersec.com.br/sinalizadores-externos/" class="icon-seta">
              <i class="path1"></i><i class="path2"></i>
              <span>saiba mais</span>
            </a>
          </div>
        </div>

        <div class="sec-block produto-2 produto-right">
          <div class="block-img">
            <img src="<?php echo get_template_directory_uri(); ?>/static/images/produto-2.png" alt="Fechaduras eletrônicas Enterse" title="Fechaduras eletrônicas Enterse">
          </div>
          <div class="block-text">
            <h4>fechaduras eletrônicas</h4>
            <p>Cada vez mais utilizada em hotéis, pousadas, escritórios e outros, a fechadura eletrônica é a garantia de segurança que o seu empreendimento precisa. Dentre seus vários benefícios, além da segurança superior, a tecnologia das fechaduras eletrônicas permite ao usuário controlar completamente os horários de entrada e saída...</p>
            <a href="http://www.entersec.com.br/fechaduras-eletronicas/" class="icon-seta">
              <i class="path1"></i><i class="path2"></i>
              <span>saiba mais</span>
            </a>
          </div>
        </div>

        <div class="sec-block produto-3">
          <div class="block-img">
            <img src="<?php echo get_template_directory_uri(); ?>/static/images/economizador-de-energia-1.png" alt="Economizadores de energia Entersec" title="Economizadores de energia Entersec">
          </div>
          <div class="block-text">
            <h4>economizadores de energia</h4>
            <p>Os economizadores de energia são dispositivos altamente sofisticados que auxiliam na redução do consumo de energia elétrica. Muito utilizado na Europa, os economizadores são cada vez mais utilizados em hotéis de todo o Brasil.</p>
            <a href="http://www.entersec.com.br/economizadores-de-energia/" class="icon-seta">
              <i class="path1"></i><i class="path2"></i>
              <span>saiba mais</span>
            </a>
          </div>
        </div>

      </div>
    </div>
  </div>
</section>