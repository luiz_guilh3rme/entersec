<section class="section depoimento-form-section">
  <div class="container">
    <div class="row">
      
      <div class="depoimento-block sec-block col-xs-12 col-sm-6 col-md-5">
        
        <h4 class="h1 block-title"><strong>Depoimentos</strong></h4>
        
        <div class="banner-content swiper-container swiper-depoimentos">
          <div class="swiper-wrapper">

            <div class="depoimento-item swiper-slide">
              <iframe width="100%" height="320" src="https://www.youtube.com/embed/o4JDQ7urOKk" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            </div>

            <div class="depoimento-item swiper-slide">
              <div class="depoiment-img">
                <img src="<?php echo get_template_directory_uri(); ?>/static/images/ibis-hotel.jpg" alt="">
                <p>
                  <strong>Renato Garcia</strong><br/>
                  Chefe de manutenção<br/>
                  IBIS Campinas
                </p>
              </div>
              <div class="depoimento-content">
                <p>“Qualidade e tecnologia de primeira. Atendimento pós venda é diferenciado, de modo geral os produtos Timelox/Entersec não trazem problemas de mau funcionamento e quase não dá manutenção.<br/>Não há nada que os desabone, recomendo. Produtos e serviços de qualidade incontestáveis.”</p>
              </div>
            </div>

            <div class="depoimento-item swiper-slide">
              <div class="depoiment-img">
                <img src="<?php echo get_template_directory_uri(); ?>/static/images/mar-palace-hotel.jpg" alt="">
                <p>
                  <strong>Hilton Lino de Andrade</strong><br/>
                  Gerente Geral<br/>
                  Mar Palace Copacabana Hotel
                </p>
              </div>
              <div class="depoimento-content">
                <p>“Não tive preocupação alguma com os produtos devido a parceria de anos oferecida pela a Entersec. Segurança e atendimento do suporte. Preços mais competitivos das chaves eletrônicas. Recomendo a Entersec pelos produtos oferecidos e a presteza de toda equipe.”</p>
              </div>
            </div>


          </div>

          <div class="swiper-depoimentos-pag"></div>
        </div>

      </div>

      <div class="form-block sec-block col-xs-12 col-sm-6 col-md-6 col-md-push-1">

        <h4 class="h1 block-title">Vamos ser <strong>parceiros</strong></h4>
        <form action="" class="form-home" id="form-home">

          <!-- NEWSP -->
          <input type="hidden" name="url" value="<?= "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>">
          <!-- TIPO DE FORM -->
          <input type="hidden" name="tipo" class="tipo" value="home">

          <div class="form-line">
            <div class="form-item">
              <input type="text" name="nome" placeholder="Nome">
            </div>
          </div>
          <div class="form-line">
            <div class="form-item">
              <input type="email" name="email" placeholder="Email">
            </div>
          </div>
          <div class="form-line">
            <div class="form-item">
              <textarea name="mensagem" id="mensagem" cols="30" rows="10" placeholder="Mensagem"></textarea>
            </div>
          </div>
          <!-- submit -->
          <div class="form-line">
            <button class="icon-seta" type="submit">
              <i class="path1"></i><i class="path2"></i>
              <span>Enviar</span>
            </button>
          </div>
        </form>

      </div>

    </div>
  </div>
</section>