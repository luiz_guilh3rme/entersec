<?php get_header(); ?>

<!-- Banner -->
<?php include('banner-interno.php') ?>
<!-- //Banner -->

<main class="main" role="main">

	<section class="section page">
		<div class="container">

			<div class="page-text col-xs-12 col-md-6">
				<h1><?php the_title(); ?></h1>

				<?php if (have_posts()): while (have_posts()) : the_post(); ?>
					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						<?php the_content(); ?>
					</article>
				<?php endwhile; ?>

				<?php else: ?>
					<!-- article -->
					<article>
						<h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>
					</article>
					<!-- /article -->
				<?php endif; ?>
			</div>
			<div class="page-form col-xs-12 col-md-6">
				<form action="" id="form-interna">

					<!-- NEWSP -->
					<input type="hidden" name="url" value="{$smarty.server.HTTP_HOST}{$smarty.server.REQUEST_URI}">
			        <!-- TIPO DE FORM -->
			        <input type="hidden" name="tipo" class="tipo" value="interna">


		          <div class="form-line">
		            <div class="form-item">
		              <input type="text" name="nome" placeholder="Nome">
		            </div>
		          </div>

		          <div class="form-line">
		            <div class="form-item">
		              <input type="email" name="email" placeholder="Email">
		            </div>
		          </div>

		          <div class="form-line">
		            <div class="form-item">
		              <input type="text" name="telefone" class="celular-input" placeholder="Telefone">
		            </div>
		          </div>

		          <div class="form-line">
		            <div class="form-item">
		              <input type="text" name="assunto" placeholder="Assunto">
		            </div>
		          </div>

		          <div class="form-line">
		            <div class="form-item">
		              <textarea name="mensagem" id="mensagem" cols="30" rows="10" placeholder="Mensagem"></textarea>
		            </div>
		          </div>

		          <!-- submit -->
		          <div class="form-line">
		            <button class="icon-seta" type="submit">
		              <i class="path1"></i><i class="path2"></i>
		              <span>Enviar</span>
		            </button>
		          </div>
		        </form>
			</div>


		</div>
	</section>

</main>

<?php get_footer(); ?>
