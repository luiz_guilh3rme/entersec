<section class="section economia-section">
  <div class="blue-section">
    <div class="container">
      <div class="row">

        <div class="col-xs-12 col-md-6 chart-block">
          <h3 class="h1">Economize <strong>energia</strong></h3>
          <img src="<?php echo get_template_directory_uri(); ?>/static/images/chart.png" alt="Economize Energia" title="Economize Energia">
        </div>

        <div class="col-xs-12 col-md-4 col-md-push-1 col-lg-3 col-lg-push-1 chart-desc-block">
          <h4>Como funciona?</h4>
          <p class="txt-branco">O Economizador de Energia Online funciona a base de um cartão de leitura que efetua a identificação de presença no quarto e libera o consumo de energia no recinto, assim como desliga o consumo imediatamente após a retirada do cartão, evitando o desperdício de energia elétrica. O economizador também realiza a leitura do cartão e fornece informações em tempo real dos quartos em que são utilizados. <br/>
          Seu uso é indicado em hotéis, pousadas, hospitais, escritórios e outros.</p>
          <a href="http://www.entersec.com.br/economizadores-de-energia/" class="icon-seta">
            <i class="path1"></i><i class="path2"></i>
            <span>saiba mais</span>
          </a>
        </div>
      </div>

      <div class="men2"></div>
    </div>
  </div>
  
</section>