<?php if( is_category() ): ?>
	
	<div class="banner-interna" role="banner">
	    <div class="banner-item" style="background-image: url('<?php echo get_template_directory_uri(); ?>/static/images/<?php echo get_category(get_query_var('cat'))->slug; ?>.jpg');">
	      <div class="seta1">
	        <img src="<?php echo get_template_directory_uri(); ?>/static/images/seta2.svg">
	      </div>
	    </div>
	</div>

<?php elseif( 'post' == get_post_type() ): ?>

	<div class="banner-interna" role="banner">

		<?php $bannerinterno = get_field('banner_interno'); ?>
		<?php $categories = get_the_category(); ?>

		<?php if( !empty($bannerinterno) ): ?>

	    	<div class="banner-item" style="background-image: url('<?php echo $bannerinterno['url']; ?>');">

		<?php else: ?>

			<div class="banner-item" style="background-image: url('<?php echo get_template_directory_uri(); ?>/static/images/<?php echo esc_html( $categories[0]->slug ); ?>.jpg');">

		<?php endif; ?>


	      <div class="seta1">
	        <img src="<?php echo get_template_directory_uri(); ?>/static/images/seta2.svg">
	      </div>
	    </div>
	</div>

<?php else: ?>

	<div class="banner-interna banner-pages" role="banner">
	    <div class="banner-item" style="background-image: url('<?php the_post_thumbnail_url(); ?>');">
	      <div class="seta1">
	        <img src="<?php echo get_template_directory_uri(); ?>/static/images/seta2.svg">
	      </div>
	    </div>
	</div>

<?php endif; ?>
