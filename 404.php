<?php get_header(); ?>

<!-- Banner -->
<?php include('banner-interno.php') ?>
<!-- //Banner -->

<main class="main" role="main">

	<section class="section page">
		<div class="container">

			<div class="page-text col-xs-12 txt">
				<h1>Página não encontrada</h1>
				<p>A página que você tentou acessar não exite</p><br/>
				<a href="<?php echo site_url(); ?>">Voltar para home</a>
			</div>

		</div>
	</section>
</main>

<?php get_footer(); ?>
