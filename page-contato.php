<?php /* Template Name: Contato */ get_header(); ?>

<!-- Banner -->
<?php include('banner-interno.php') ?>
<!-- //Banner -->

<main class="main" role="main">

	<section class="section page">
		<div class="container">

			<div class="page-text col-xs-12 col-md-6">
				<h1><?php the_title(); ?></h1>

				<?php if (have_posts()): while (have_posts()) : the_post(); ?>
					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						<?php the_content(); ?>
					</article>
				<?php endwhile; ?>

				<?php else: ?>
					<!-- article -->
					<article>
						<h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>
					</article>
					<!-- /article -->
				<?php endif; ?>
			</div>
			<div class="page-form col-xs-12 col-md-6">
				<form action="" id="form-interna">

					<!-- NEWSP -->
					<input type="hidden" name="url" value="<?= "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>">
			        <!-- TIPO DE FORM -->
			        <input type="hidden" name="tipo" class="tipo" value="interna">
			        
		          <div class="form-line">
		            <div class="form-item">
		              <input type="text" name="nome" placeholder="Nome">
		            </div>
		          </div>

		          <div class="form-line">
		            <div class="form-item">
		              <input type="email" name="email" placeholder="Email">
		            </div>
		          </div>

		          <div class="form-line">
		            <div class="form-item">
		              <input type="text" name="telefone" class="celular-input" placeholder="Telefone">
		            </div>
		          </div>

		          <div class="form-line">
		            <div class="form-item">
		              <input type="text" name="assunto" placeholder="Assunto">
		            </div>
		          </div>

		          <div class="form-line">
		            <div class="form-item">
		              <textarea name="mensagem" id="mensagem" cols="30" rows="10" placeholder="Mensagem"></textarea>
		            </div>
		          </div>

		          <!-- submit -->
		          <div class="form-line">
		            <button class="icon-seta" type="submit">
		              <i class="path1"></i><i class="path2"></i>
		              <span>Enviar</span>
		            </button>
		          </div>
		        </form>
			</div>



		</div>
	</section>

	<div class="map">
		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3659.437556705906!2d-46.567782085023765!3d-23.480744184722933!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94cef58f4801e261%3A0xfe3230ccd56dbdfd!2sRua+Hintem+Martins%2C+467+-+Parque+Edu+Chaves%2C+S%C3%A3o+Paulo+-+SP%2C+02229-070!5e0!3m2!1spt-BR!2sbr!4v1561395001855!5m2!1spt-BR!2sbr" width="100%" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
	</div>
</main>

<?php get_footer(); ?>
